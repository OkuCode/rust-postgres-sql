extern crate postgres;

use postgres::{Client, NoTls, Error};

#[derive(Debug)]
struct Facultad {
    id_fac: String,
    nom_fac: String,
    dec_fac: String
}

fn main() -> Result<(), Error> {

    // postgres = User;
    // 123456 = password;
    // localhost = host machine;
    // 5243 = port
    // ejemplo = database;

    let mut client = Client::connect(
        "postgresql://postgres:123456@localhost:5243/ejemplo",
        NoTls)?;

    //Inserting data

    client.execute("insert into facultad (id_fac, nom_fac, dec_fac) values ($1, $2, $3)"
        , &[&"fac06", &"facultad de biologia", &"Hernesto Hernandez"] )?;
    
    for row in client.query("select id_fac, nom_fac, dec_fac from facultad", &[])? {
        let facultad = Facultad {
            id_fac: row.get(0),
            nom_fac: row.get(1),
            dec_fac: row.get(2)
        };

        println!("Faculdades: {:?}", facultad);
    }

    Ok(())
}
